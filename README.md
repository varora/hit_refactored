## Description
![Python](https://img.shields.io/badge/Python->=3.8-Blue?logo=python) 
![Pytorch](https://img.shields.io/badge/Pytorch->=1.13.1-Blue?logo=pytorch)

This repository contains the refactored segmentation of dicoms code for training and predicting segmented classes for HIT project.

## Tested using
- conda (version : 23.3.1)
- python (version : 3.8)
- platform : linux-64 (Ubuntu 22.04.2 LTS)

## Installation
Follow these steps to set up the conda environment and run the code:
1. Clone the repository:
`git clone https://gitlab.inria.fr/varora/hit_refactored.git`
2. Change into the project directory
3. Create a new conda environment:
`conda create -n hit python=3.8`
4. Activate the conda environment:
`conda activate hit`
5. Install the required packages:
```pip install -r requirements.txt```

## Pre-requisites
- The train/val data resides in `/media/veracrypt1/Data/mri_segmentation/goldeen/mri_paired_final/`

## Expected folder structure:
```
root_path
├── requirements.txt
├── README.md
├── train.py
├── eval.py
├── utils
│   ├── __init__.py
│   ├── log.py
│   ├── losses.py
│   ├── misc.py
│   ├── visualizations.py    
├── model
│   ├── __init__.py
│   ├── unet_model.py
│   ├── unet_parts.py
├── data_loader
│   ├── __init__.py
│   ├── data_loader.py
```
After running train.py and eval.py, additional `train_outputs` and `eval_outputs` folders will be created in the root_path.
```
├── train_outputs
│   ├── exp_name
│       ├── checkpoints
│       │    ├── CP_epoch_1.pth
│       │    ├── CP_epoch_2.pth
│       │    ├── ...
│       ├── exp_name.log
│       ├── tensorboard_events
├── eval_outputs
│   ├── output_folder_name
│   │    ├── sub_1
│   │    ├── sub_2
│   │    ├── ...
│   │    ├── sub_N
│   ├── Dice score_box_plots.png
│   ├── errors_box_plots_without_outliers.png
│   ├── errors.json
```

## Usage
#### Train:
```
python train.py -e 50 -b 10 -cl 3 -lr 8e-4 -s 1 -se 10 -en <train-experiment-name>
```
#### Evaluate:
```
python eval.py -o ./eval_outputs/<output-folder-name>/ -m ./train_outputs/<train-experiment-name>/checkpoints/CP_final.pth -cl 3 -s 1 --true_mask -i /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/CD1490/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/EL1021/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/GM1150/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/HS891/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/JF1444/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/RB323/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/RM1249/dcm/ /media/veracrypt1/Data/mri_segmentation/data_split_manual/mri_paired/test/SP1494/dcm/
```
