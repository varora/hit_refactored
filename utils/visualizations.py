import matplotlib.pyplot as plt
import numpy as np

plt.figure()


def plot_img_and_mask(img, mask, classes=None, channels=1, true_mask=None, save=False, loss=None, single_plot=False):
    plt.close("all")

    if classes is None:
        classes = mask.shape[0] if len(mask.shape) > 2 else 1
    if true_mask is None:
        if channels > 1:
            fig, ax = plt.subplots(channels, classes + 1)
            fig.set_size_inches(5 * (classes + 1), 5 * (channels))
            for j in range(channels):
                ax[j, 0].set_title('Input image (number {})'.format(j + 1))
                ax[j, 0].imshow(img[j])
                if classes > 1:
                    for i in range(classes):
                        ax[j, i + 1].set_title('Output mask (class {})'.format(i + 1))
                        ax[j, i + 1].imshow(mask[i, :, :])
                else:
                    ax[j, 1].set_title('Output mask')
                    ax[j, 1].imshow(mask[j, :, :])
        else:
            fig, ax = plt.subplots(1, classes + 1)
            fig.set_size_inches(5 * (classes + 1), 5)
            ax[0].set_title('Input image')
            ax[0].imshow(img)
            if classes > 1:
                for i in range(classes):
                    ax[i + 1].set_title('Output mask (class {})'.format(i + 1))
                    ax[i + 1].imshow(mask[i, :, :])
            else:
                ax[1].set_title('Output mask')
                ax[1].imshow(mask)
    else:
        if channels > 1:
            fig, ax = plt.subplots(2 * channels, 2 * classes + 1)
            if loss is not None:
                if isinstance(loss, float):
                    fig.suptitle('Total loss before binarization: {}'.format(loss), fontsize=14)
                else:
                    fig.suptitle('Total loss before binarization: {}'.format(loss[4]) + '\n' +
                                 'Loss per classes: 1 class: {}, 2 class: {}, 3 class: {}'.format(loss[-3], loss[-2],
                                                                                                  loss[-1]) + '\n' +
                                 'LT and AT intersection loss: {}, LT and AT (pixels): {}'.format(loss[1],
                                                                                                  loss[3]) + '\n' +
                                 'LT-AT union difference with ALL loss: {}, LT-AT union difference with ALL (pixels): {}'.format(
                                     loss[0], loss[2]) + '\n' +
                                 'VAT-AT difference: {}, VAT-AT difference (pixels): {}'.format(loss[4], loss[5]),
                                 fontsize=14)
            fig.set_size_inches(5 * (2 * classes + 1), 5 * (2 * channels))
            for j in range(channels):
                ax[2 * j, 0].set_title('Input image (number {})'.format(j + 1))
                ax[2 * j, 0].imshow(img[j])
                ax[2 * j + 1, 0].set_title('Input image (number {})'.format(j + 1))
                ax[2 * j + 1, 0].imshow(img[j])
                if classes > 1:
                    for i in range(classes):
                        ax[2 * j, 2 * i + 1].set_title('Output mask (class {})'.format(i + 1))
                        ax[2 * j, 2 * i + 1].imshow(mask[3 * j + i, :, :])
                        ax[2 * j + 1, 2 * i + 1].set_title('True mask (class {})'.format(i + 1))
                        ax[2 * j + 1, 2 * i + 1].imshow(true_mask[3 * j + i, :, :])
                        ax[2 * j, 2 * i + 2].set_title('False positive (FP) (class {})'.format(i + 1))
                        FP = np.where(np.logical_and(true_mask[3 * j + i, :, :] == 0, mask[3 * j + i, :, :] > 0.5),
                                      mask[3 * j + i, :, :], np.zeros(mask[3 * j + i, :, :].shape))
                        mask_without_FP = np.where(FP > 0, np.zeros(mask[3 * j + i, :, :].shape), mask[3 * j + i, :, :])
                        ax[2 * j, 2 * i + 2].imshow(
                            np.dstack([FP, np.zeros(mask[3 * j + i, :, :].shape), mask_without_FP]))
                        ax[2 * j + 1, 2 * i + 2].set_title('False negative (FN) (class {})'.format(i + 1))
                        FN = np.where(np.logical_and(true_mask[3 * j + i, :, :] == 1, mask[3 * j + i, :, :] < 0.5),
                                      true_mask[3 * j + i, :, :], np.zeros(mask[3 * j + i, :, :].shape))
                        mask_without_FN = np.where(FN > 0, np.zeros(true_mask[3 * j + i, :, :].shape),
                                                   true_mask[3 * j + i, :, :])
                        ax[2 * j + 1, 2 * i + 2].imshow(
                            np.dstack([np.zeros(mask[3 * j + i, :, :].shape), FN, mask_without_FP]))
                else:
                    ax[2 * j, 1].set_title('Output mask')
                    ax[2 * j, 1].imshow(mask[j, :, :])
                    ax[2 * j + 1, 1].set_title('True mask')
                    ax[2 * j + 1, 1].imshow(true_mask[j, :, :])
                    ax[2 * j, 2].set_title('False positive (FP)')
                    FP = np.where(np.logical_and(true_mask[j, :, :] == 0, mask[j, :, :] > 0.5), mask[j, :, :],
                                  np.zeros(mask[j, :, :].shape))
                    mask_without_FP = np.where(FP > 0, np.zeros(mask[j, :, :].shape), mask[j, :, :])
                    ax[2 * j, 2].imshow(np.dstack([FP, np.zeros(mask[j, :, :].shape), mask_without_FP]))
                    ax[2 * j + 1, 2].set_title('False negative (FN)')
                    FN = np.where(np.logical_and(true_mask[j, :, :] == 1, mask[j, :, :] < 0.5), mask[j, :, :],
                                  np.zeros(mask[j, :, :].shape))
                    mask_without_FN = np.where(FN > 0, np.zeros(true_mask[j, :, :].shape), true_mask[j, :, :])
                    ax[2 * j + 1, 2].imshow(np.dstack([np.zeros(mask[j, :, :].shape), FN, mask_without_FP]))
        else:
            fig, ax = plt.subplots(2, classes + 1)
            if loss is not None:
                if isinstance(loss, float):
                    fig.suptitle('Total loss before binarization: {}'.format(loss), fontsize=14)
                else:
                    # fig.suptitle('Total loss before binarization: {}'.format(loss[4]) + '\n' +
                    #                 'Loss per classes: 1 class: {}, 2 class: {}, 3 class: {}'.format(loss[-3], loss[-2],loss[-1]) + '\n' +
                    #                 'LT and AT intersection loss: {}, LT and AT (pixels): {}'.format(loss[1], loss[3])+ '\n'+
                    #                 'LT-AT union difference with ALL loss: {}, LT-AT union difference with ALL (pixels): {}'.format(loss[0], loss[2])+'\n'+
                    #                 'VAT-AT difference: {}, VAT-AT difference (pixels): {}'.format(loss[4], loss[5]), fontsize=14)
                    fig.suptitle('Total loss before binarization: {}'.format(loss[4]) + '\n' +
                                 'Loss per classes: 1 class: {}, 2 class: {}, 3 class: {}'.format(loss[-3], loss[-2],
                                                                                                  loss[-1]),
                                 fontsize=14)
            ax[0, 0].set_title('Input image')
            ax[0, 0].imshow(img)
            ax[1, 0].set_title('Input image')
            ax[1, 0].imshow(img)

            if classes > 1:
                fp_list = []
                fn_list = []

                fig.set_size_inches(7 * classes, 7.5)
                fig.subplots_adjust(hspace=0.00001, wspace=0.12)
                for i in range(classes):
                    # ax[0,i+1].set_title('Output mask (class {})'.format(i+1))
                    # ax[0,i+1].imshow(mask[i,:, :])
                    # ax[1,i+1].set_title('True mask (class {})'.format(i+1))
                    # ax[1,i+1].imshow(true_mask[i,:, :])
                    ax[0, i + 1].set_title('Output mask\nand false positive (FP) (class {})'.format(i + 1))
                    FP = np.where(np.logical_and(true_mask[i, :, :] == 0, mask[i, :, :] > 0.5), mask[i, :, :],
                                  mask[i, :, :] * 0.5)
                    FP[0, 0] = 1
                    ax[0, i + 1].imshow(FP)
                    fp_list.append(FP)
                    ax[1, i + 1].set_title('True mask\nand false negative (FN) (class {})'.format(i + 1))
                    FN = np.where(np.logical_and(true_mask[i, :, :] == 1, mask[i, :, :] <= 0.5), true_mask[i, :, :],
                                  true_mask[i, :, :] * 0.5)
                    FN[0, 0] = 1
                    ax[1, i + 1].imshow(FN)
                    fn_list.append(FN)
            else:
                fig.set_size_inches(7 * classes, 7 * classes)
                fig.subplots_adjust(hspace=0.01, wspace=0.15)
                # ax[0,1].set_title('Output mask')
                # ax[0,1].imshow(mask)
                # ax[1,1].set_title('True mask')
                # ax[1,1].imshow(true_mask)
                ax[0, 1].set_title('Output mask\nand false positive (FP)')
                FP = np.where(np.logical_and(true_mask == 0, mask > 0.5), mask, mask * 0.5)
                FP[0, 0] = 1
                ax[0, 1].imshow(FP)
                ax[1, 1].set_title('True mask\nand false negative (FN)')
                FN = np.where(np.logical_and(true_mask == 1, mask <= 0.5), true_mask, true_mask * 0.5)
                FN[0, 0] = 1
                ax[1, 1].imshow(FN)

    plt.xticks([]), plt.yticks([])
    if not save:
        plt.show()
    if not single_plot:
        return fig
    else:
        return fig, img, fp_list, fn_list
