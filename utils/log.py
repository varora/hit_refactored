import logging
import os
import sys


def block_print():
    """
    Block printing to stdout.
    :return: void
    """
    sys.stdout = open(os.devnull, 'w')


def enable_print():
    """
    Enable printing to stdout.
    :return: void
    """
    sys.stdout = sys.__stdout__


def set_logging(filename):
    """
    Set up logging.
    :param filename: the filename to log to
    :return: void
    """
    logging.basicConfig(filename=filename, level=logging.INFO, force=True)


def log_print(message, print_to_console=True):

    # Log the message
    logging.info(message)

    # Print the message to the console if enabled
    if print_to_console:
        print(message)


def print_arguments(args):
    """
    Print the arguments.
    :param args: the arguments
    :return: void
    """
    log_print("Arguments:")
    for arg in vars(args):
        log_print("\t" + arg + ": " + str(getattr(args, arg)))