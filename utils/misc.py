from glob import glob
from os.path import join
import numpy as np
import xlrd

def normalize_image_uint8(img):
    min_val = np.min(img)
    max_val = np.max(img)
    span = max_val - min_val
    if span == 0:
        span = 1

    return ((img.astype(float) - min_val) / span * 255).astype('uint8')


def find_slice_change(sid_folder):
    ''' This parses the folder containing a single xls
        and finds the cut value between upper and lower
    '''

    xl_filename = glob(join(sid_folder, '*.xls'))[0]
    print(xl_filename)

    xl = xlrd.open_workbook(xl_filename)

    xl_sheet = xl.sheet_by_index(0)

    nb_id = xl_sheet.col(5)

    a = np.argmin(np.array([x.value for x in nb_id]))

    # print 'Change between ', a + 5, ' and ', a + 6

    return a + 5