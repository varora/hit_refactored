import torch
import numpy as np

def dice_loss(pred, target):
    """
        pred(X) : predicted sets   -> range[0, 1]
        target(Y): ground truth     -> binary labels {0, 1}
        DSC = 2|X.Y|/(|X|+|Y|)
        X.Y: intersection | true positive | number of overlappig labels
        |X|+|Y|: union
        |X|, |Y|: the number of labels in each set

    """
    # import torch.nn as nn
    # m = nn.Sigmoid()
    eps = 1e-5

    input_ = torch.flatten(pred)
    target_ = torch.flatten(target)

    intersec = torch.sum(input_ * target_)
    union = torch.sum(input_) + torch.sum(target_)

    dsc = (2.0 * intersec + eps) / (union + eps)

    return 1 - dsc


def dice_coeff(pred, target):
    # return s / (i + 1)
    eps = 1e-5

    intersec = torch.sum(pred * target)
    union = torch.sum(pred) + torch.sum(target)

    dsc = (2.0 * intersec + eps) / (union + eps) if (intersec != 0 or union != 0) else 1

    return dsc



def reverse_size(im, shape):
    if shape == (352, 704):
        img_ = im[4:88 + 4, :]
        img = cv2.resize(img_, shape)
    elif shape == (176, 256):
        img_ = im[4:88 + 4, 24:128 + 24]
        img = cv2.resize(img_, shape)
    elif shape == (256, 192):
        img = im
    else:
        img = im
        # img_ = im[:, 24:128+24]
        # img = cv2.resize(img_, shape)
    return img


def compute_dice(im1, im2):
    # @varora: np.bool to bool
    """im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)"""
    im1 = np.asarray(im1).astype(bool)
    im2 = np.asarray(im2).astype(bool)

    eps = 0.00001

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: {} and {}".format(im1.shape, im2.shape))

    intersec = np.logical_and(im1, im2).sum()
    union = im1.sum() + im2.sum()

    result = (2. * intersec + eps) / (union + eps) if (intersec != 0 or union != 0) else 1

    return result


def compute_dice_float(im1, im2):
    im1 = np.asarray(im1)
    im2 = np.asarray(im2)

    eps = 0.00001

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: {} and {}".format(im1.shape, im2.shape))

    intersec = (im1 * im2).sum()
    union = np.sum(im1) + np.sum(im2)

    result = (2.0 * intersec + eps) / (union + eps) if (intersec != 0 or union != 0) else 1

    return result


def compute_dice_per_class_float(im1, im2):
    im1 = np.asarray(im1)
    im2 = np.asarray(im2)

    eps = 0.000000001

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: {} and {}".format(im1.shape, im2.shape))

    result = []
    for i in range(im1.shape[0]):
        intersec = (im1[i] * im2[i]).sum()
        union = np.sum(im1[i]) + np.sum(im2[i])
        result.append((2.0 * intersec + eps) / (union + eps))

    return np.array(result)


def compute_dice_per_class(im1, im2):
    # @varora: np.bool to bool
    """im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)"""
    im1 = np.asarray(im1).astype(bool)
    im2 = np.asarray(im2).astype(bool)

    eps = 0.000000001

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: {} and {}".format(im1.shape, im2.shape))

    result = []
    for i in range(im1.shape[0]):
        intersec = np.logical_and(im1[i], im2[i]).sum()
        union = np.sum(im1[i]) + np.sum(im2[i])
        # print(im1,im2, intersec, union, np.sum(im1), np.sum(im2))
        result.append((2.0 * intersec + eps) / (union + eps))

    return np.array(result)