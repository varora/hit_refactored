import logging
import os
from glob import glob

import cv2
import numpy as np
import torch
from PIL import Image
from pydicom.filereader import read_file
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from UNet.utils.misc import normalize_image_uint8


class ComplexDataset(Dataset):
    def __init__(self, imgs_list, masks_list, labeled_list=None, scale=1, mask_suffix=''):
        assert len(imgs_list) == len(masks_list), 'Numbers of images and masks must be same'
        self.imgs_list = imgs_list
        self.masks_list = masks_list
        if labeled_list is None:
            self.labeled = [True for i in masks_list]
        else:
            self.labeled = labeled_list
        self.scale = scale
        self.mask_suffix = mask_suffix
        assert 0 < scale <= 1, 'Scale must be between 0 and 1'
        # self.ids = [dirname(file_).split('/')[-2]+'/'+basename(file_).split('.')[0] for file_ in imgs_list]
        logging.info('Creating dataset with {} examples'.format({len(self.imgs_list)}))

    def __len__(self):
        return len(self.imgs_list)

    @classmethod
    def preprocess(cls, pil_imgs, scale):

        if not isinstance(pil_imgs, list):
            tmp = []
            tmp.append(pil_imgs)
            pil_imgs = tmp
        imgs = []
        for pil_img in pil_imgs:
            w, h = pil_img.size
            newW, newH = int(scale * w), int(scale * h)
            # print(newW, newH)
            assert newW > 0 and newH > 0, 'Scale is too small'
            pil_img = pil_img.resize((newW, newH))
            img_ = np.array(pil_img)

            if img_.shape == (176, 352):
                img_ = cv2.resize(img_, (176, 88))
                img_ = cv2.copyMakeBorder(img_, 4, 4, 0, 0, cv2.BORDER_CONSTANT)
            elif img_.shape == (176, 256):
                img_ = cv2.resize(img_, (128, 88))
                img_ = cv2.copyMakeBorder(img_, 4, 4, 24, 24, cv2.BORDER_CONSTANT)
            elif img_.shape == (96, 128):
                img_ = cv2.copyMakeBorder(img_, 0, 0, 24, 24, cv2.BORDER_CONSTANT)
            elif img_.shape == (352, 704):
                img_ = cv2.resize(img_, (512, 352))

            else:
                # raise ValueError('Not known shape of image')
                img_ = img_
            # print(img_.shape)
            imgs.append(img_)

        img_nd = np.dstack(imgs)

        if len(img_nd.shape) == 2:
            img_nd = np.expand_dims(img_nd, axis=2)

        # HWC to CHW
        img_trans = img_nd.transpose((2, 0, 1)).astype(dtype=np.float32)
        if img_trans.max() > 1:
            img_trans = img_trans / 255

        return img_trans

    def __getitem__(self, i):

        img_files = self.imgs_list[i]
        labeled = self.labeled[i]

        mask_files = self.masks_list[i]

        img = []
        if not isinstance(img_files, str):
            for img_file in img_files:
                img_ = normalize_image_uint8(read_file(img_file).pixel_array)

                img.append(Image.fromarray(img_))
        else:
            img_ = normalize_image_uint8(read_file(img_files).pixel_array)

            img.append(Image.fromarray(img_))

        img = self.preprocess(img, self.scale)

        if labeled:
            mask = []
            for mask_file in mask_files:
                mask.append(Image.open(mask_file))
            mask = self.preprocess(mask, self.scale)
        else:
            shape = (96, 176)
            img_nd = np.dstack([np.zeros(shape) - 1 for mask_file in mask_files])
            if len(img_nd.shape) == 2:
                img_nd = np.expand_dims(img_nd, axis=2)

            # HWC to CHW
            mask = img_nd.transpose((2, 0, 1)).astype(dtype=np.float32)
        # print('img: ', img.shape, 'mask: ', mask.shape)

        return {
            'image': torch.from_numpy(img).type(torch.FloatTensor),
            'mask': torch.from_numpy(mask).type(torch.FloatTensor),
            'labeled': labeled
        }


def get_dataset_1_channel(n_classes, img_scale, validation=0.0, dir_='', val_dir_=''):
    if n_classes == 1:
        raise ValueError('Not implemented')

    elif n_classes == 3:
        files_dirs_list = os.walk(dir_)
        d = {}

        for path, dirs, files in files_dirs_list:

            if len(dirs) == 0:
                tmp = path.split('/')[0]
                # print(path)
                for directory in path.split('/')[1:-1]:
                    tmp = os.path.join(tmp, directory)
                if path.endswith('VAT'):
                    # @varora : bad fix to correct dir path
                    if tmp[0] != '/':
                        tmp = '/' + tmp
                    f = glob(os.path.join(tmp, 'VAT', '*.png'))
                    f.sort()
                    for vat in f:
                        if np.sum(cv2.imread(vat)) == 0:
                            f.remove(vat)
                    if d.get(tmp) is None:
                        d.update({tmp: {'VAT': f,
                                        'dcm': [
                                            f_.replace("/VAT/", "/dcm/").replace(".png", ".dcm").replace("VAT", "ALL")
                                            for f_ in f],
                                        'LT': [f_.replace("VAT", "LT") for f_ in f],
                                        'AT': [f_.replace("VAT", "AT") for f_ in f],
                                        'ALL': [f_.replace("VAT", "ALL") for f_ in f]}})

        list_img = []
        list_mask = []
        for tmp, files_dict in d.items():
            imgs = files_dict.get('dcm')
            ALL = files_dict.get('ALL')
            LT = files_dict.get('LT')
            AT = files_dict.get('AT')
            VAT = files_dict.get('VAT')
            masks = []
            for i in range(len(LT)):
                masks.append([LT[i], AT[i], VAT[i]])
            list_img.extend(imgs)
            list_mask.extend(masks)
        list_labeled = [True for i in list_img]

        # dataset = BasicDataset(list_img, list_mask, img_scale)
        # print(list_img, list_mask)
        val_files_dirs_list = os.walk(val_dir_)
        val_d = {}

        for path, dirs, files in val_files_dirs_list:
            if len(dirs) == 0:
                tmp = path.split('/')[0]
                for directory in path.split('/')[1:-1]:
                    tmp = os.path.join(tmp, directory)
                if path.endswith('VAT'):
                    # @varora : bad fix to correct dir path
                    if tmp[0] != '/':
                        tmp = '/' + tmp
                    f = glob(os.path.join(tmp, 'VAT', '*.png'))
                    f.sort()
                    for vat in f:
                        if np.sum(cv2.imread(vat)) == 0:
                            f.remove(vat)
                    if val_d.get(tmp) is None:
                        val_d.update({tmp: {'VAT': f,
                                            'dcm': [f_.replace("/VAT/", "/dcm/").replace(".png", ".dcm").replace("VAT",
                                                                                                                 "ALL")
                                                    for f_ in f],
                                            'LT': [f_.replace("VAT", "LT") for f_ in f],
                                            'AT': [f_.replace("VAT", "AT") for f_ in f],
                                            'ALL': [f_.replace("VAT", "ALL") for f_ in f]}})

        # val_dataset = BasicDataset(val_list_img, val_list_mask, img_scale)

        val_list_img = []
        val_list_mask = []
        for tmp, files_dict in val_d.items():
            imgs = files_dict.get('dcm')
            ALL = files_dict.get('ALL')
            LT = files_dict.get('LT')
            AT = files_dict.get('AT')
            VAT = files_dict.get('VAT')
            masks = []
            for i in range(len(LT)):
                masks.append([LT[i], AT[i], VAT[i]])
            val_list_img.extend(imgs)
            val_list_mask.extend(masks)
        val_list_labeled = [True for i in val_list_img]

        dataset = ComplexDataset(list_img, list_mask, list_labeled, img_scale)
        val_dataset = ComplexDataset(val_list_img, val_list_mask, val_list_labeled, img_scale)

        return dataset, val_dataset


def data_loaders(dir_, val_dir_, img_scale=1.0, batch_size=8, n_channels=1, validation=0.0, n_classes=3):
    """
    :param img_scale:
    :param batch_size:
    :param n_channels:
    :param validation:
    :param dir_:
    :param val_dir_:
    :return:
    data loader for train and validation
    """
    if n_channels == 1:
        dataset, val_dataset = get_dataset_1_channel(n_classes, img_scale, validation, dir_, val_dir_)
    elif n_channels == 3:
        raise ValueError('Not implemented')
    train_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True,
                              drop_last=True)
    if val_dataset is not None:
        val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True,
                                drop_last=True)
        return train_loader, val_loader
    return train_loader, None


class BasicDataset(Dataset):
    def __init__(self, imgs_list, masks_list, labeled_list=None, scale=1, mask_suffix=''):
        print(len(imgs_list))
        print(len(masks_list))
        assert len(imgs_list) == len(masks_list), 'Numbers of images and masks must be same'
        self.imgs_list = imgs_list
        self.masks_list = masks_list
        if labeled_list is None:
            self.labeled = [True for i in masks_list]
        else:
            self.labeled = labeled_list
        self.scale = scale
        self.mask_suffix = mask_suffix
        assert 0 < scale <= 1, 'Scale must be between 0 and 1'
        self.ids = [dirname(file_).split('/')[-2] + '/' + basename(file_).split('.')[0] for file_ in imgs_list]
        logging.info('Creating dataset with {} examples'.format({len(self.ids)}))

    def __len__(self):
        return len(self.ids)

    @classmethod
    def preprocess(cls, pil_img, scale):
        w, h = pil_img.size
        newW, newH = int(scale * w), int(scale * h)
        assert newW > 0 and newH > 0, 'Scale is too small'
        pil_img = pil_img.resize((newW, newH))

        img_nd = np.array(pil_img)

        if img_nd.shape == (176, 352):
            img_nd = cv2.resize(img_nd, (176, 88))
            img_nd = cv2.copyMakeBorder(img_nd, 4, 4, 0, 0, cv2.BORDER_CONSTANT)
        elif img_nd.shape == (176, 256):
            img_nd = cv2.resize(img_nd, (128, 88))
            img_nd = cv2.copyMakeBorder(img_nd, 4, 4, 24, 24, cv2.BORDER_CONSTANT)
        elif img_nd.shape == (96, 128):
            img_nd = cv2.copyMakeBorder(img_nd, 0, 0, 24, 24, cv2.BORDER_CONSTANT)
        else:
            # raise ValueError('Not known shape of image')
            img_nd = img_nd

        if len(img_nd.shape) == 2:
            img_nd = np.expand_dims(img_nd, axis=2)

        # HWC to CHW
        img_trans = img_nd.transpose((2, 0, 1)).astype(dtype=np.float32)
        if img_trans.max() > 1:
            img_trans = img_trans / 255

        return img_trans

    def __getitem__(self, i):
        idx = self.ids[i]
        mask_file = self.masks_list[i]
        img_file = self.imgs_list[i]
        labeled = self.labeled[i]

        # assert dirname(mask_file).split('/')[-2]+'/'+basename(mask_file).split('.')[0] == dirname(img_file).split('/')[-2]+'/'+basename(img_file).split('.')[0], \
        #    'Image and mask should have the same name, but are {} and {}'.format(dirname(img_file).split('/')[-2]+'/'+basename(img_file).split('.')[0],dirname(mask_file).split('/')[-2]+'/'+basename(mask_file).split('.')[0])
        # assert dirname(mask_file).split('/')[-2]+'/'+basename(mask_file).split('.')[0] == idx, \
        #    'Image {} is not correspond to mask {}'.format(idx,dirname(mask_file).split('/')[-2]+'/'+basename(mask_file).split('.')[0])
        # assert dirname(img_file).split('/')[-2]+'/'+basename(img_file).split('.')[0] == idx, \
        #    'Image {} is not correspond to the image with the name {}'.format(idx,dirname(img_file).split('/')[-2]+'/'+basename(img_file).split('.')[0])

        # assert len(mask_file) == 1, \
        #    f'Either no mask or multiple masks found for the ID {idx}: {mask_file}'
        # assert len(img_file) == 1, \
        #    f'Either no image or multiple images found for the ID {idx}: {img_file}'
        mask = Image.open(mask_file)
        img = normalize_image_uint8(read_file(img_file).pixel_array)

        img = Image.fromarray(img)

        img = self.preprocess(img, self.scale)
        mask = self.preprocess(mask, self.scale)

        return {
            'image': torch.from_numpy(img).type(torch.FloatTensor),
            'mask': torch.from_numpy(mask).type(torch.FloatTensor),
            'labeled': labeled
        }