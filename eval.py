import argparse
import logging
import os
from re import L
from matplotlib import pyplot as plt
from glob import glob

from os.path import splitext, basename, dirname, isdir, join

import numpy as np
import torch
from PIL import Image
from torchvision import transforms

from model import UNet
from utils.visualizations import plot_img_and_mask
from data_loader.data_loader import ComplexDataset, BasicDataset
from utils.misc import normalize_image_uint8, find_slice_change
from pydicom.filereader import read_file
from utils.losses import compute_dice, compute_dice_float, compute_dice_per_class, compute_dice_per_class_float, reverse_size

import json
import cv2
import warnings

warnings.filterwarnings("ignore")


def predict_img(net,
                full_img,
                device,
                scale_factor=1,
                out_threshold=0.5,
                ALL=None):
    net.eval()

    img = torch.from_numpy(BasicDataset.preprocess(full_img, scale_factor))


    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output, _ = net(img)

        probs = torch.sigmoid(output)

        probs = probs.squeeze(0)

        full_mask = probs.squeeze().cpu().numpy()

    mask = full_mask > out_threshold


    LT_AT_intersec = 0
    LT_AT_union = 0
    LTAT_ALL_difference = 0
    VAT_AT_difference = 0
    LT_mask = mask[0, :, :]
    AT_mask = mask[1, :, :]
    VAT_mask = mask[2, :, :]

    eps = 0.00001

    if ALL is not None:
        ALL_mask = ALL[0, :, :]

    for x in range(LT_mask.shape[0]):
        for y in range(LT_mask.shape[1]):
            # print(LT_mask[x,y], AT_mask[x,y] , ALL_mask[x,y])
            if LT_mask[x, y] == True and AT_mask[x, y] == True:
                LT_AT_intersec += 1

            if VAT_mask[x, y] == True and AT_mask[x, y] == False:
                VAT_AT_difference += 1

            if (LT_mask[x, y] == True or AT_mask[x, y] == True):
                LT_AT_union += 1

            if ALL is not None:
                if (LT_mask[x, y] == True or AT_mask[x, y] == True) and ALL_mask[x, y] == False:
                    LTAT_ALL_difference += 1

                if (LT_mask[x, y] == False and AT_mask[x, y] == False) and ALL_mask[x, y] == True:
                    LTAT_ALL_difference += 1

    ALL_sum = np.sum(ALL_mask)
    return mask, full_mask, [LTAT_ALL_difference * 1.0 / (ALL_sum + eps),
                             LT_AT_intersec * 1.0 / (LT_AT_union + eps), LTAT_ALL_difference, LT_AT_intersec,
                             VAT_AT_difference * 1.0 / (np.sum(VAT_mask) + eps), VAT_AT_difference]


def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model', '-m', default='MODEL.pth',
                        metavar='FILE',
                        help="Specify the file in which the model is stored")
    parser.add_argument('--input', '-i', metavar='INPUT', nargs='+',
                        help='filenames of input images', required=True)

    parser.add_argument('--output', '-o', metavar='INPUT', nargs='+',
                        help='Filenames of ouput images')
    parser.add_argument('--viz', '-v', action='store_true',
                        help="Visualize the images as they are processed",
                        default=False)
    parser.add_argument('--no-save', '-n', action='store_true',
                        help="Do not save the output masks",
                        default=False)
    parser.add_argument('--mask-threshold', '-t', type=float,
                        help="Minimum probability value to consider a mask pixel white",
                        default=0.5)
    parser.add_argument('--scale', '-s', type=float,
                        help="Scale factor for the input images",
                        default=0.5)
    parser.add_argument('-ch', '--n_channels', dest='n_channels', type=int, default=1,
                        help='Number of input channels. Default = 1')
    parser.add_argument('--seed', dest='seed', type=int, default=1,
                        help='For which image in stack we are doing prediction (0,1,2). Default = 1 (middle)')
    parser.add_argument('-cl', '--n_classes', dest='n_classes', type=int, default=1,
                        help='Number of output channels (classes). Default = 1')
    parser.add_argument('--true_mask', '-tr', action='store_true',
                        help="Visualize the true mask",
                        default=False)

    return parser.parse_args()


def get_output_filenames(in_name):
    if not args.output:
        out_dir = join(os.getcwd(), 'output')
        try:
            os.makedirs(out_dir)
            logging.info('Created output directory')
        except OSError:
            pass
        idx = dirname(in_name).split('/')[-2]
        try:
            os.makedirs(join(out_dir, idx))
        except OSError:
            pass
        pathsplit = basename(in_name).split('.')
        out_name = join(out_dir, idx, "{}_OUT{}".format(pathsplit[0], '.png'))
    else:
        try:
            os.makedirs(args.output[0])
            logging.info('Created output directory')
        except OSError:
            pass
        idx = dirname(in_name).split('/')[-2]
        try:
            os.makedirs(join(args.output[0], idx))
        except OSError:
            pass
        pathsplit = basename(in_name).split('.')
        out_name = join(args.output[0], idx, "{}_OUT{}".format(pathsplit[0], '.png'))

    return out_name


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))


def get_true_mask(img_name, n_classes, scale_factor, n_channels):
    ALL_path = img_name.replace("/dcm/", "/ALL/")
    ALL_path = ALL_path.replace("dcm", "png")
    LT_path = ALL_path.replace("ALL", "LT")
    AT_path = ALL_path.replace("ALL", "AT")
    VAT_path = ALL_path.replace("ALL", "VAT")
    masks = []
    for mask_file in [LT_path, AT_path, VAT_path]:
        masks.append(Image.open(mask_file))
    mask = ComplexDataset.preprocess(masks, scale_factor)
    ALL = BasicDataset.preprocess(Image.open(ALL_path), scale_factor)
    return mask, ALL


if __name__ == "__main__":
    args = get_args()
    in_files = []
    in_files_parts = []

    flag = False
    for inp in args.input:
        if isdir(inp):
            if not flag:
                c = 4
                print('No of body parts', c)
                flag = True
            if not args.true_mask:
                try:
                    os.makedirs(inp.replace("/dicoms/", "/ALL/"))
                except OSError:
                    pass
                try:
                    os.makedirs(inp.replace("/dicoms/", "/LT/"))
                except OSError:
                    pass
                try:
                    os.makedirs(inp.replace("/dicoms/", "/AT/"))
                except OSError:
                    pass
                try:
                    os.makedirs(inp.replace("/dicoms/", "/VAT/"))
                except OSError:
                    pass

            im_list = glob(join(inp, '*.dcm'))
            im_list.sort()

            in_files.extend(im_list)
            for j in range(c - 1):
                for i in range(j * len(im_list) // c, (j + 1) * len(im_list) // c):
                    in_files_parts.append(j)
            for i in range((c - 1) * len(im_list) // c, len(im_list)):
                in_files_parts.append(c - 1)
        else:
            in_files.append(inp)

    in_files_parts = np.array(in_files_parts)
    eval_dice_loss = []
    eval_dice_loss_float = []
    eval_dice_loss_per_class = []
    eval_dice_loss_float_per_class = []
    dependancies_losses_pixels = []

    net = UNet(n_channels=args.n_channels, n_classes=args.n_channels * args.n_classes)
    logging.info("Loading model {}".format(args.model))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info('Using device {}'.format(device))

    net.to(device=device)

    net.load_state_dict(torch.load(args.model, map_location=device))

    logging.info("Model loaded !")
    print('Things are working')
    for i, fn in enumerate(in_files):
        if args.n_channels == 1:
            logging.info("\nPredicting image {} ...".format(fn))

            img = normalize_image_uint8(read_file(fn).pixel_array)

            img = Image.fromarray(img)
            if args.true_mask:
                true_mask, ALL = get_true_mask(fn, args.n_classes, args.scale, args.n_channels)
            else:
                true_mask = None
                ALL = None

        mask, full_mask, dependancies_errors = predict_img(net=net,
                                                           full_img=img,
                                                           scale_factor=args.scale,
                                                           out_threshold=args.mask_threshold,
                                                           device=device, ALL=ALL)
        if not args.no_save:
            out_name = get_output_filenames(fn)
            if not args.true_mask:
                # varora: not used
                print('Things are working perfect')
                result = plot_img_and_mask(ComplexDataset.preprocess(img, args.scale)[0, :, :], full_mask,
                                           classes=args.n_classes, channels=net.n_channels, save=True)

                shape = img.size

                ####################################################
                ALL_path = fn.replace("/dicoms/", "/ALL/")
                # print(os.path.split(ALL_path)[:-1][0])
                PATH = os.path.join(os.path.split(ALL_path)[:-1][0], 'ALL', os.path.split(ALL_path)[-1])
                # ALL_path = ALL_path.replace("/dicoms/", "/ALL/")

                # print('Path is now', PATH)
                ALL_path = PATH.replace("dcm", "png")

                LT_path = ALL_path.replace("ALL", "LT")
                AT_path = ALL_path.replace("ALL", "AT")
                VAT_path = ALL_path.replace("ALL", "VAT")

                print('Path is', LT_path)
                # print("Path status",os.path.exists(os.path.split(ALL_path)[:-1][0]))

                if not os.path.exists(os.path.split(ALL_path)[:-1][0]):
                    os.mkdir(os.path.split(ALL_path)[:-1][0])
                    os.mkdir(os.path.split(LT_path)[:-1][0])
                    os.mkdir(os.path.split(AT_path)[:-1][0])
                    os.mkdir(os.path.split(VAT_path)[:-1][0])
                ##########################################################

                Image.fromarray((255 * reverse_size(full_mask[0, :, :], shape)).astype(np.int8)).convert('L').save(
                    LT_path)
                Image.fromarray((255 * reverse_size(full_mask[1, :, :], shape)).astype(np.int8)).convert('L').save(
                    AT_path)
                Image.fromarray((255 * reverse_size(full_mask[2, :, :], shape)).astype(np.int8)).convert('L').save(
                    VAT_path)
                Image.fromarray((255 * reverse_size(np.maximum(full_mask[0, :, :], full_mask[1, :, :]), shape)).astype(
                    np.int8)).convert('L').save(ALL_path)
            else:
                eval_dice_loss.append(compute_dice(np.array(mask), true_mask))
                eval_dice_loss_float.append(compute_dice_float(np.array(full_mask), true_mask))
                eval_dice_loss_per_class.append(compute_dice_per_class(np.array(mask), true_mask))
                eval_dice_loss_float_per_class.append(compute_dice_per_class_float(np.array(full_mask), true_mask))
                dependancies_losses_pixels.append(
                    [dependancies_errors[2], dependancies_errors[3], dependancies_errors[5]])

                all_losses = []
                all_losses.extend(dependancies_errors)
                all_losses.append(eval_dice_loss_float[-1])
                all_losses.extend(eval_dice_loss_per_class[-1].tolist())

                result = plot_img_and_mask(ComplexDataset.preprocess(img, args.scale)[0, :, :], full_mask,
                                           classes=args.n_classes, channels=net.n_channels, true_mask=true_mask,
                                           save=True, loss=all_losses)

                shape = img.size

                idx = dirname(fn).split('/')[-2]
                ALL_path = join(args.output[0], idx, "ALL", basename(fn))
                ALL_path = ALL_path.replace("dcm", "png")
                LT_path = ALL_path.replace("ALL", "LT")
                AT_path = ALL_path.replace("ALL", "AT")
                VAT_path = ALL_path.replace("ALL", "VAT")

                print("Path status", os.path.exists(os.path.split(ALL_path)[:-1][0]))
                if not os.path.exists(os.path.split(ALL_path)[:-1][0]):
                    os.mkdir(os.path.split(ALL_path)[:-1][0])
                    os.mkdir(os.path.split(LT_path)[:-1][0])
                    os.mkdir(os.path.split(AT_path)[:-1][0])
                    os.mkdir(os.path.split(VAT_path)[:-1][0])

                Image.fromarray((255 * reverse_size(full_mask[0, :, :], shape)).astype(np.int8)).convert('L').save(
                    LT_path)
                Image.fromarray((255 * reverse_size(full_mask[1, :, :], shape)).astype(np.int8)).convert('L').save(
                    AT_path)
                Image.fromarray((255 * reverse_size(full_mask[2, :, :], shape)).astype(np.int8)).convert('L').save(
                    VAT_path)
                Image.fromarray(
                    (255 * reverse_size(np.maximum(full_mask[0, :, :], full_mask[1, :, :]), shape)).astype(
                        np.int8)).convert('L').save(ALL_path)
                print('image: ', fn, ', dice loss before binarization: ', eval_dice_loss_float[-1])
            result.savefig(out_name)
            logging.info("Mask saved to {}".format(out_name))
            print("Mask saved to {}".format(out_name))

        if args.viz:
            logging.info("Visualizing results for image {}, close to continue ...".format(fn))

            if not args.true_mask:
                plot_img_and_mask(ComplexDataset.preprocess(img, args.scale), mask, classes=args.n_classes,
                                  channels=net.n_channels)
            else:
                eval_dice_loss.append(compute_dice(np.array(mask), true_mask))
                eval_dice_loss_float.append(compute_dice_float(np.array(full_mask), true_mask))
                eval_dice_loss_per_class.append(compute_dice_per_class(np.array(mask), true_mask))
                eval_dice_loss_float_per_class.append(compute_dice_per_class_float(np.array(full_mask), true_mask))
                plot_img_and_mask(ComplexDataset.preprocess(img, args.scale), full_mask, classes=args.n_classes,
                                  channels=net.n_channels, true_mask=true_mask, loss=eval_dice_loss_float[-1])

    if args.true_mask:
        with open(join(args.output[0], 'errors.json'), 'w') as f:
            json.dump({'errors before binarization': eval_dice_loss_float,
                       'errors after binarization': eval_dice_loss,
                       'images list': in_files,
                       'images body parts': in_files_parts.tolist()}, f)

    if args.true_mask and not args.no_save:
        fig, ax = plt.subplots(args.n_classes + 1, 4)
        eval_dice_loss_float = np.array(eval_dice_loss_float)
        eval_dice_loss = np.array(eval_dice_loss)
        eval_dice_loss_float_per_class = np.array(eval_dice_loss_float_per_class)
        eval_dice_loss_per_class = np.array(eval_dice_loss_per_class)
        ax[0, 0].set_title('Dice score')
        ax[0, 1].set_title('Dice score over body parts')
        ax[0, 0].boxplot(eval_dice_loss_float)
        errors_parts_float = [eval_dice_loss_float[np.where(in_files_parts == j)] for j in range(c)]
        ax[0, 1].boxplot(errors_parts_float)
        ax[0, 2].set_title('Dice score after binarization')
        ax[0, 3].set_title('Dice score after binarization over body parts')
        ax[0, 2].boxplot(eval_dice_loss)
        errors_parts = [eval_dice_loss[np.where(in_files_parts == j)] for j in range(c)]

        ax[0, 3].boxplot(errors_parts)

        for i in range(1, args.n_classes + 1):
            errors_parts_float_all = []
            errors_parts_all = []
            ax[i, 0].set_title('Dice score, class {}'.format(i - 1))
            ax[i, 1].set_title('Dice score over body parts, class {}'.format(i - 1))
            ax[i, 0].boxplot(eval_dice_loss_float_per_class[:, i - 1])

            for j in range(c):
                errors_parts_float = []
                errors_parts = []
                j_part = np.where(in_files_parts == j)
                for k in range(j_part[0].shape[0]):
                    errors_parts_float.append(eval_dice_loss_float_per_class[j_part[0][k], i - 1])
                    errors_parts.append(eval_dice_loss_per_class[j_part[0][k], i - 1])
                errors_parts_float_all.append(errors_parts_float)
                errors_parts_all.append(errors_parts)

            ax[i, 1].boxplot(errors_parts_float_all)
            ax[i, 2].set_title('Dice score after binarization, class {}'.format(i - 1))
            ax[i, 3].set_title('Dice score after binarization over body parts, class {}'.format(i - 1))
            ax[i, 2].boxplot(eval_dice_loss_per_class[:, i - 1])

            ax[i, 3].boxplot(errors_parts_all)
        fig.set_size_inches(20, 14)
        fig.savefig(join(args.output[0], 'Dice score_box_plots.png'))

    if args.true_mask and not args.no_save:
        fig, ax = plt.subplots(args.n_classes + 1, 4)
        eval_dice_loss_float = np.array(eval_dice_loss_float)
        eval_dice_loss = np.array(eval_dice_loss)
        eval_dice_loss_float_per_class = np.array(eval_dice_loss_float_per_class)
        eval_dice_loss_per_class = np.array(eval_dice_loss_per_class)
        ax[0, 0].set_title('Dice score')
        ax[0, 1].set_title('Dice score over body parts')
        ax[0, 0].boxplot(eval_dice_loss)
        errors_parts = [eval_dice_loss[np.where(in_files_parts == j)] for j in range(c)]
        ax[0, 1].boxplot(errors_parts)
        ax[0, 2].set_title('Dice score (without outliers)', size=10)
        ax[0, 3].set_title('Dice score (without outliers) over body parts', size=10)
        ax[0, 2].boxplot(eval_dice_loss, showfliers=False)
        ax[0, 3].boxplot(errors_parts, showfliers=False)

        for i, name in zip(range(1, args.n_classes + 1), ['LT', 'AT', 'VAT']):
            errors_parts_float_all = []
            ax[i, 0].set_title('Dice score, {} class'.format(name), size=10)
            ax[i, 1].set_title('Dice score over body parts, {} class'.format(name), size=10)
            ax[i, 0].boxplot(eval_dice_loss_per_class[:, i - 1])
            print('Evaluation Dice loss ({} class): '.format(i),
                  np.average(eval_dice_loss_float_per_class[:, i - 1]))
            print('Evaluation Dice loss after binarization ({} class): '.format(i),
                  np.average(eval_dice_loss_per_class[:, i - 1]))

            for j in range(c):
                errors_parts_float = []
                j_part = np.where(in_files_parts == j)
                for k in range(j_part[0].shape[0]):
                    errors_parts_float.append(eval_dice_loss_per_class[j_part[0][k], i - 1])
                errors_parts_float_all.append(errors_parts_float)

            ax[i, 1].boxplot(errors_parts_float_all)
            ax[i, 2].set_title('Dice score (without outliers), {} class'.format(name), size=10)
            ax[i, 3].set_title('Dice score (without outliers) over body parts, {} class'.format(name), size=10)
            ax[i, 2].boxplot(eval_dice_loss_per_class[:, i - 1], showfliers=False)

            ax[i, 3].boxplot(errors_parts_float_all, showfliers=False)
        fig.set_size_inches(20, 14)
        fig.subplots_adjust(hspace=0.2, wspace=0.2)
        fig.savefig(join(args.output[0], 'errors_box_plots_without_outliers.png'))
        dependancies_losses_pixels = np.array(dependancies_losses_pixels)
        print('LT AT intersection (pixels): ', np.average(dependancies_losses_pixels[:, 0]))
        print('LT AT ALL difference (pixels): ', np.average(dependancies_losses_pixels[:, 1]))
        print('VAT AT difference (pixels): ', np.average(dependancies_losses_pixels[:, 2]))

        print('Evaluation Dice loss: ', np.average(eval_dice_loss))
        print('Evaluation Dice loss before binarization: ', np.average(eval_dice_loss_float))