import argparse
import os

import torch
import torch.nn as nn
from torch import optim
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from model import UNet

from UNet.utils.losses import dice_coeff, dice_loss
from UNet.data_loader.data_loader import data_loaders
from utils.log import set_logging, log_print, print_arguments

dir_ = '/media/veracrypt1/Data/mri_segmentation/goldeen/mri_paired_final/train'
val_dir_ = '/media/veracrypt1/Data/mri_segmentation/goldeen/mri_paired_final/validate'

for folder in [dir_, val_dir_]:
    if os.path.exists(folder):
        log_print("{0} exists".format(folder))
    else:
        raise FileNotFoundError("{0} not found".format(folder))


def eval_net(net, loader, device):
    """Evaluation without the densecrf with the dice coefficient"""
    net.eval()
    mask_type = torch.float32 if net.n_classes == 1 else torch.long
    n_val = len(loader)  # the number of batch
    tot = 0

    with tqdm(total=n_val, desc='Validation round', unit='batch', leave=False) as pbar:
        for batch in loader:
            imgs, true_masks = batch['image'], batch['mask']
            imgs = imgs.to(device=device, dtype=torch.float32)
            true_masks = true_masks.to(device=device, dtype=mask_type)

            with torch.no_grad():
                mask_pred, _ = net(imgs)

            pred = torch.sigmoid(mask_pred)

            loss = dice_coeff(pred=pred, target=true_masks)

            tot += loss.item()
            pbar.update()

    net.train()
    return tot / n_val


def train_net(net,
              device,
              epochs=5,
              batch_size=1,
              lr=0.001,
              save_cp=True,
              img_scale=0.5,
              loss_type='Dice',
              alpha=0.2,
              validation=0.0,
              save_every=0,
              save_dir=None,
              ):
    checkpoint_dir = os.path.join(save_dir, 'checkpoints')
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)
    # @varora : validation = 0 always? n_channels = 1 always?
    train_loader, val_loader = data_loaders(
        dir_, val_dir_,
        img_scale=img_scale, batch_size=batch_size, n_channels=net.n_channels, validation=validation, n_classes=net.n_classes
    )
    if val_loader is None:
        log_print("Validation dataset not specified. Training on train dataset only.")
        log_print("No of train samples: {}".format(train_loader.dataset.__len__()))
    else:
        log_print("No of train samples: {}".format(train_loader.dataset.__len__()))
        log_print("No of val samples: {}".format(val_loader.dataset.__len__()))

    writer = SummaryWriter(log_dir=save_dir)
    global_step = 0

    log_print('''Starting training:
        Epochs:          {}
        Batch size:      {}
        Learning rate:   {}
        Checkpoints:     {}
        Device:          {}
        Images scaling:  {}
        Loss type:       {}
    '''.format(epochs, batch_size, lr, save_cp, device.type, img_scale, loss_type))

    optimizer = optim.Adam(net.parameters(), lr=lr)

    sigm = nn.Sigmoid()

    for epoch in range(epochs):
        net.train()

        epoch_loss = 0
        with tqdm(total=train_loader.dataset.__len__(), desc='Epoch {}/{}'.format(epoch, epochs),
                  unit='img') as pbar:
            n_train = len(train_loader)  # the number of batch
            train_ep_dice_loss = 0
            for batch in train_loader:
                optimizer.zero_grad()

                # model inputs
                img = batch['image'].cuda()
                real_msk = batch['mask'].cuda()

                img = img.to(device=device, dtype=torch.float32)
                mask_type = torch.float32 if net.n_classes == 1 else torch.long
                real_msk = real_msk.to(device=device, dtype=mask_type)

                # apply the model
                pred, _ = net(img)

                # print('img: ', img.size(), 'mask: ', real_msk.size(), ', pred: ', pred.size())

                pred = sigm(pred)

                loss = dice_loss(pred=pred, target=real_msk)  # returns 1 - dice_coeff per batch
                writer.add_scalar('Dice loss/train', loss.item(), global_step)

                loss.backward()

                nn.utils.clip_grad_value_(net.parameters(), 1)
                optimizer.step()

                epoch_loss += loss.item()

                pbar.update(batch['image'].shape[0])

                global_step += 1

                writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], global_step)
                train_ep_dice_loss += loss.item()

        train_score = 1 - (train_ep_dice_loss / n_train)
        log_print('Epoch {} Train Dice Coeff: {}'.format(epoch, train_score))
        writer.add_scalar('Dice coeff/train', train_score, epoch)

        val_score = eval_net(net, val_loader, device)  # returns average dice coeff
        log_print('Epoch {} Validation Dice Coeff: {}\n'.format(epoch, val_score))
        writer.add_scalar('Dice coeff/validation', val_score, epoch)

        if save_every!=0 and epoch % save_every == 0:
            torch.save(net.state_dict(),
                       os.path.join(checkpoint_dir, 'CP_epoch{}.pth'.format(epoch)))
            log_print('Checkpoint {} saved !'.format(epoch))

    torch.save(net.state_dict(), os.path.join(checkpoint_dir, 'CP_final.pth'))
    writer.flush()
    writer.close()


def get_args():
    parser = argparse.ArgumentParser(description='Train the UNet on images and target masks',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-e', '--epochs', metavar='E', type=int, default=5,
                        help='Number of epochs', dest='epochs')
    parser.add_argument('-b', '--batch-size', metavar='B', type=int, nargs='?', default=20,
                        help='Batch size', dest='batchsize')
    parser.add_argument('-lr', '--learning-rate', metavar='LR', type=float, nargs='?', default=0.001,
                        help='Learning rate', dest='lr')
    parser.add_argument('-f', '--load', dest='load', type=str, default=False,
                        help='Load model from a .pth file')
    parser.add_argument('-s', '--scale', dest='scale', type=float, default=0.5,
                        help='Downscaling factor of the images')
    parser.add_argument('-v', '--validation', dest='validation', type=float, default=0,
                        help='Percent of the data that is used as validation (0-100)')
    parser.add_argument('-l', '--loss', dest='loss', type=str, default='Dice',
                        help='Used loss: Dice; Dice+L1; Dice+L2. Default = Dice')
    parser.add_argument('-a', '--alpha', dest='alpha', type=float, default=0.2,
                        help='Coefficient of L1 or L2 loss. Default = 0.2')
    parser.add_argument('-ch', '--n_channels', dest='n_channels', type=int, default=1,
                        help='Number of input channels. Default = 1')
    parser.add_argument('-cl', '--n_classes', dest='n_classes', type=int, default=1,
                        help='Number of output channels (classes). Default = 1')
    parser.add_argument('-se', '--save_every', dest='save_every', type=int, default=0,
                        help='Save every n epochs. Default = 1')
    parser.add_argument('-en', '--experiment_name', dest='experiment_name', type=str, default=None)

    return parser.parse_args()


if __name__ == '__main__':
    args = get_args()

    # create an output directory if it does not exist
    output_root = 'train_outputs'
    if not os.path.exists(output_root):
        os.makedirs(output_root)

    experiment_name = args.experiment_name
    if experiment_name is None:
        # get current timestamp
        import datetime
        now = datetime.datetime.now()
        experiment_name = now.strftime("%Y-%m-%d_%H-%M-%S")

    output_dir = os.path.join(output_root, experiment_name)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    log_filename = experiment_name + '.log'
    set_logging(filename=os.path.join(output_dir, log_filename))

    print_arguments(args)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    log_print('\nUsing device {}'.format(device))

    # n_channels=3 for RGB images
    # n_classes is the number of probabilities you want to get per pixel
    #   - For 1 class and background, use n_classes=1
    #   - For 2 classes, use n_classes=1
    #   - For N > 2 classes, use n_classes=N

    # model: n_channels=1, n_classes=3
    unet = UNet(n_channels=args.n_channels, n_classes=args.n_channels * args.n_classes, bilinear=True)

    scaling_type = "Bilinear" if unet.bilinear else "Transposed conv"
    log_print("\nNetwork:\n"
                 "\t {} input channels\n".format(unet.n_channels) +
                 "\t {} output channels (classes)\n".format(unet.n_classes) +
                 '\t{} upscaling'.format(scaling_type))

    if args.load:
        unet.load_state_dict(
            torch.load(args.load, map_location=device)
        )
        log_print("Model loaded from {}".format(args.load))

    unet.to(device=device)

    # faster convolutions, but more memory
    # cudnn.benchmark = True

    try:
        train_net(net=unet,
                  epochs=args.epochs,
                  batch_size=args.batchsize,
                  lr=args.lr,
                  device=device,
                  img_scale=args.scale,
                  loss_type=args.loss,
                  alpha=args.alpha,
                  validation=args.validation,
                  save_every=args.save_every,
                  save_dir=output_dir)

    except KeyboardInterrupt:
        # torch.save(net.state_dict(), 'INTERRUPTED.pth')
        # log_print('Saved interrupt')
        print('Interrupted')
